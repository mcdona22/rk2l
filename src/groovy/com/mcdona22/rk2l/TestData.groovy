package com.mcdona22.rk2l


class TestData {
    int id
    Date timeStamp
    String name
    List<TestItem> tests


    String toString(){
        "{id:$id, timeStamp:$timeStamp, name:$name, tests:$tests}"
    }
}
