package com.mcdona22.rk2l

/**
 * Created with IntelliJ IDEA.
 * User: mcdona22
 * Date: 12/06/2013
 * Time: 07:25
 * To change this template use File | Settings | File Templates.
 */
class DeviceTestContainer {
    int testSetCount
    List<Range> ranges = []
    List<String> lines = []
    List<TestData> testData = []
    String filename = ""
    String deviceId = ""

    String toString(){
        "{filename: $filename, deviceId: $deviceId, range:$ranges}"
    }
}
