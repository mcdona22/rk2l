package rk2l

import com.mcdona22.rk2l.DeviceTestContainer
import com.mcdona22.rk2l.TestData
import com.mcdona22.rk2l.TestItem

import static org.apache.commons.lang.StringUtils.*
import static org.apache.commons.lang.Validate.isTrue
import static org.apache.commons.lang.Validate.notNull


class FileProcessorService {
    final static String DATE_FORMAT =  "yyyy-MM-dd hh:mm:ss"
    final static int HDR_LINES = 5
    final String TEST_DELIM = "SubTestID: "
    final String TEST_SET_DELIM = "Test ID:"
    final static String TEST_END_TOKEN  = "Test Overall Result"


    protected TestData parseTestData(List<String> lines){
        TestData data = new TestData(tests: [])

        readHeader(lines, data)

        int totalLines = lines.size() -1
        def range = 5..totalLines
        //println "the range is $range"
        List remainingLines = lines[HDR_LINES..totalLines]

        while(hasNext(remainingLines)){
            println "yes - we have another test"
            Map linesList = getTestLines(remainingLines)
            remainingLines = linesList.remainingLines
            //println "The found test"
            //println linesList["testLines"]
            //println "The remaining lines"
            //println linesList["remainingLines"]

            List testLines = linesList["testLines"]

            //println "here we are"
            String currentTestName = substringAfter(testLines[1], TEST_DELIM)
            // this may be more robust using a regex - but for now lets use this and deal with that case later
            currentTestName = substringBefore(currentTestName, ".").trim()
            TestItem currentTest = new TestItem(name: currentTestName)
            currentTest.result = substringAfter(testLines[4], "Result:").trim()
            currentTest.desc = substringAfter(testLines[3], "Desc:").trim()
            testLines = testLines[5..testLines.size() - 1]
            def trace =   join(testLines, "\n")
            trace = substringAfter(trace, "Trace:").trim()
            currentTest.trace = trace

            data.tests << currentTest
        }


        return data
    }

    protected TestData readHeader (List<String> lines) {

        String dateString = lines[0]
        String idString = substringAfter(lines[2], "_id: ")
        String name = substringAfterLast(lines[1], "Test ID: ")
        name = substringBefore(name, ": ")
        int id = Integer.parseInt(idString)
        Date date = Date.parse(DATE_FORMAT, dateString)
        //data.name = name
        TestData data = new TestData(tests: [], name: name, timeStamp: date, id: id)
        return data

    }

    protected DeviceTestContainer prepare(File file) {
        notNull(file, "The file argument may not be null")
        isTrue(file.isFile(), "The file ${file} cannot be reached")

        DeviceTestContainer set  = new DeviceTestContainer(filename: file.name, deviceId: file.parentFile.name)


        set.lines = file.readLines()

        set.lines.remove(0)  // this is just the 'logging started' line and makes all the tests consistent in pattern now
        int currentIndex = 0

        def lineCount = set.lines.size() - 1
        int currentRangeStart = 0
        int currentRangeEnd = 0
        boolean beginningFound = false

        lineCount.times{ i ->
            if(set.lines[i].startsWith(TEST_END_TOKEN)){
                currentRangeEnd = i - 2
                set.ranges << (currentRangeStart..currentRangeEnd)
                //set.ranges << (currentIndex..i)
                //currentIndex = i + 1   // beginning of the next range - NO!
            }

            beginningFound = set.lines[i].startsWith(TEST_SET_DELIM)

            if (beginningFound){
                // we are into another test - set the beginning of the range
                currentRangeStart = i - 1 // the previous line has the date for this set
            }
        }

        return set
    }

    protected List<Range> getIndividualTestRanges(List<String> testLines ) {
        isTrue(testLines.size() > 0, "The set of lines for extracting tests must be valid")
        List<Range> ranges = []
        List testStartIndices = []
        testLines.size().times{i ->
            // println(testLines[i])
            if(startsWith(testLines[i], "SubTestID:")){
                ////println "woohoo - found the test start - ${testLines[i]} recording ${i - 1}"
                testStartIndices << (i - 1) // the date preceding it is part of the test
                //println testStartIndices
                //ranges << (1..10)
            }
        }
        //println "creating the ranges"
        testStartIndices.size().times{ i ->

        //for( int i in testStartIndices){
            // the start of the range is as recorded.  The end is 1 before the next start or its
            // the last one in which case deduct n lines for the footer of the test set
            int rangeStart = testStartIndices[i]
            int rangeEnd = (i == testStartIndices.size() - 1) ?
                    testLines.size() - 1           // the offset to exclude at the end of the set of tests
                    : testStartIndices[i + 1] - 1   // the line preceding the date

            //println testLines[i]
            //println testLines[i + 1]
            Range r = rangeStart..rangeEnd
            ranges << (rangeStart..rangeEnd)
        }
        //println ranges
        return ranges
    }

    protected TestItem parseTest(List<String> testLines) {
        // all these items can be found consistently in the representation using simple
        // convention of prefixes.  The trace is everything after 'Trace:'
        String name = chomp(substringAfter(testLines[1], TEST_DELIM), ":")
        String result = substringAfter(testLines[4], "Result: ")
        String desc = substringAfter(testLines[3], "Desc:")
        // the remaining lines are the trace join back up with a newline
        def traceLines =  testLines[5..(testLines.size() -1 )]
        String trace = join(traceLines, "\n" )
        trace = strip(substringAfter(trace, "Trace:"))
        TestItem item = new TestItem(name: name.trim(), result: result, desc: desc, trace: trace)
    }

    DeviceTestContainer processFile(File file) {
        notNull(file, "The file argument may not be null")
        isTrue(file.isFile(), "The file argument for '${file.name}' is unreachable")

        DeviceTestContainer testContainer = prepare(file)
        int count = 0
        testContainer.ranges.each{ range ->
            List<String> lines = testContainer.lines[range.from..range.to]
            TestData testData = readHeader(lines)
            testContainer.testData << testData
            def individualTestRanges = getIndividualTestRanges(lines)
            individualTestRanges.each{r ->
                def testLines =  lines[r.from..r.to]
                TestItem item =  parseTest(testLines)
                testData.tests << item
            }
        }

        return testContainer
    }

}
