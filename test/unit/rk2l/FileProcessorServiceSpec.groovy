package rk2l

import com.mcdona22.rk2l.TestData
import com.mcdona22.rk2l.TestItem
import spock.lang.Specification
import static org.easymock.EasyMock.*

class FileProcessorServiceSpec extends Specification{
    File file
    FileProcessorService service
    TestData result
    int id =  2131034125
    String testDate = "2013-02-18 15:59:58"
    String testName = "WiFiRadioTests"
    String testDesc = "Wi-Fi Radio on"
    String testResult = "OK"
    String name = "WiFi Signal"
    String test2Desc = "Private networks"


def testTrace = '''List of BT APs:
SSID=BTOpenzone BSSID=00:11:5c:d1:57:61 Signal=-51 capabilities=[ESS]

List of all APs:
SSID=NGH_OP_BT__SP_BT_ATT BSSID=58:bc:27:12:d3:71 Signal=-29
SSID=aspire004 BSSID=20:68:9d:92:12:b1 Signal=-35
SSID=RM86ADSL BSSID=00:26:50:a4:2f:31 Signal=-40
SSID=development1 BSSID=f8:d1:11:c2:19:f4 Signal=-47
SSID=Exhall BSSID=0a:18:0a:36:00:d8 Signal=-58
SSID=Customer Centre BSSID=06:18:0a:36:00:d8 Signal=-58
SSID=APCC BSSID=00:18:0a:36:00:d8 Signal=-58
SSID=AR0adrunner BSSID=00:11:5c:d1:57:60 Signal=-55
SSID=BTHub3-JFZN BSSID=00:fe:f4:36:b9:88 Signal=-89
SSID=BTOpenzone BSSID=00:11:5c:d1:57:61 Signal=-51
SSID=BAT-Mesh BSSID=68:ef:bd:9f:87:af Signal=-73
SSID=cobalt BSSID=00:03:93:e9:b1:46 Signal=-81'''

    def testText =
"""$testDate
SubTestID: $name:
    Part: 3/8
    Desc:$test2Desc
    Result: $testResult
    Trace:
$testTrace"""

// end of big text set-up declarations -----------------------------------------

    def setup(){
        file = createNiceMock(File)
        service = new FileProcessorService()
    }

    def "when reading from file throws an exception if the argument is null" (){
        when:
            service.prepare(null)
        then:
            thrown IllegalArgumentException
    }


    def "when reading from file throws an exception if the file is not a file" (){
        setup:
            expect(file.isFile()).andReturn(false)
            replay file
        when:
            service.prepare(file)
        then:
           thrown IllegalArgumentException
    }

    def "should throw an exception if you attempt to get tests from empty set of lines"(){
        when:
            service.getIndividualTestRanges([])
        then:
            thrown IllegalArgumentException
    }

    def "should be able to parse an individual test from a given set of lines" (){
        setup:
            def lines = testText.readLines()
        when:
            TestItem testItem = service.parseTest lines
        then:
            assert name == testItem.name
            assert testResult == testItem.result
            assert test2Desc == testItem.desc
            assert testTrace == testItem.trace
    }

    def "should throw an exception when processing a file if that file cant be reached"(){
        setup:
            final String FILENAME = 'test-file-name-alpha'
            expect(file.isFile()).andReturn(false)
            expect(file.name).andReturn(FILENAME)
            replay file
        when:
            service.processFile(file)
        then:
            IllegalArgumentException e = thrown()
            e.message.contains(FILENAME)
    }

    def "should throw an exception when processing a file if that file is null"(){
        when: service.processFile(null)
        then: thrown IllegalArgumentException
    }
}
