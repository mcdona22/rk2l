package rk2l

import com.mcdona22.rk2l.DeviceTestContainer
import com.mcdona22.rk2l.TestData
import com.mcdona22.rk2l.TestItem
import spock.lang.Specification
import static org.apache.commons.lang.StringUtils.*

class FileProcessorServiceIntegrationSpec extends Specification {
    static final String TEST_DATA_ROOT = "data"

    static final String TEST_FILE = "WiFiConnectionTest__2013-03-06-131127.txt"
    static final String DEVICE_ID = "5381"
    FileProcessorService service
    private File dataFile
    private List<String> lines

    def setup(){
        dataFile = new File("${TEST_DATA_ROOT}/$DEVICE_ID/$TEST_FILE")
        service = new FileProcessorService()

    }

    def "should read the contents of a file removing the initial line"(){
        setup:
            DeviceTestContainer testSet = new DeviceTestContainer()
            int lineCount = dataFile.readLines().size()
        when:
            testSet = service.prepare(dataFile)
        then :
            testSet.lines?.size() == lineCount -1
            ! testSet.lines[0].contains("Logging started")
            println "The line count is ${testSet.lines.size()}"
    }


    def "should list the filename and device of the file that was processed"(){
        setup:
            DeviceTestContainer testSet
        when:
            testSet = service.prepare(dataFile)
        then:
            testSet.filename == TEST_FILE
            testSet.deviceId ==  DEVICE_ID
            println testSet
    }

    def "should be able to read the test header from a set of tests in a processed file"(){
        setup:
            String testDate = "2013-03-06 13:11:27"
            Date d = Date.parse(FileProcessorService.DATE_FORMAT, testDate)
            int expectedId  = 2131034125
            DeviceTestContainer testSet = service.prepare(dataFile)
            Range r = testSet.ranges[0]
            List<String> lines = testSet.lines[r.from..r.to]
        when:
            TestData testData = service.readHeader(lines)

        then:
            assert lines.size() > 0
            assert testData?.timeStamp == d
            assert testData.id == expectedId
            assert testData.name == "WiFi Signal"
            assert testData.tests?.size() == 0
            println testData

    }

    def "should be able to read the ranges of tests for a given set of lines representing a test set"(){
        setup:
            int expectedTestCount = 8
            DeviceTestContainer set = service.prepare(dataFile)
            Range r = set.ranges[0]
            List<String> lines = set.lines[r.from..r.to]
        when:
            List<Range> ranges = service.getIndividualTestRanges(lines)

        then:
            ranges?.size() == expectedTestCount
            ranges.each{
                // the first line of each test should be a date as a coarse thing
                Date d = Date.parse(FileProcessorService.DATE_FORMAT, lines[it.from])
                String firstLine = lines[it.from]
                String lastLine = lines[it.to]
                println "From ($firstLine) to ('$lastLine')"
            }

    }



    def "should be able to process a well formed file to arrive at the container level data"(){
        setup:
            int expectedTestDataSize = 4
        when:
            DeviceTestContainer container = service.processFile(dataFile)
        then:
            assert container != null
            assert container.filename == TEST_FILE
            assert container.deviceId == DEVICE_ID
            assert expectedTestDataSize == container.testData.size()
    }

    def "should process a well formed file to populate the containers TestData items"(){
        setup:
            def testSetNames = ["WiFi Signal", "WiFi Configuration", "IP Configuration", "Internet Access"]
            int expectedTestDataSize = testSetNames.size()
        when:
            DeviceTestContainer container = service.processFile(dataFile)
        then:
            assert expectedTestDataSize == container.testData.size()
            (expectedTestDataSize).times{ i ->
                println "looking for '${testSetNames[i]}' in ${container.testData[i].name}"
                assert testSetNames[i] == container.testData[i].name
                assert container.testData[i].timeStamp
            }
    }


    def "should process a file and each test set has the correct number of tests"(){
        setup:
            def testCounts = [8, 3, 6, 7]
        when:
            DeviceTestContainer container = service.processFile(dataFile)
        then:
            assert container.testData.size() == testCounts.size()
            testCounts.size().times{i ->
                assert container.testData[i].tests.size() == testCounts[i]
            }

    }

    def "should process a file and the first test in the first test set should be correct"(){
        setup:
            TestItem expected = new TestItem(desc: "Wi-Fi Radio on", result: "OK",
                    name: "WiFiRadioTests.1:", trace: "" )
        when:
            DeviceTestContainer container = service.processFile(dataFile)
            TestItem found = container.testData[0].tests[0]
        then:
            assert expected.name == found.name
            assert expected.desc == found.desc
            assert expected.result == found.result
            assert expected.trace == found.trace
    }

    def "should process a file and the last test of the final test set should be as expected"(){
        setup:
            String expectedName = "WiFiGlobalTests.7:"
            String expectedDesc = "Packet Size Test"
            String expectedResult = "OK"
            String expectedTrace =
'''Ping response successfully received.

ping -c 3 -s 83 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 83(111) bytes of data.
72 bytes from 8.8.8.8: icmp_seq=1 ttl=42 (truncated)
72 bytes from 8.8.8.8: icmp_seq=2 ttl=42 (truncated)
72 bytes from 8.8.8.8: icmp_seq=3 ttl=42 (truncated)

--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 20.600/25.380/30.701/4.145 ms'''
            DeviceTestContainer container = service.processFile(dataFile)
            int lastTestSetIndex = container.testData.size() - 1
            int lastTestIndex = container.testData[lastTestSetIndex].tests.size() -1
            expectedTrace = trim(expectedTrace)
        when:
            TestItem found = container.testData[lastTestSetIndex].tests[lastTestIndex]
        then:
            assert found.name == expectedName
            assert found.desc == expectedDesc
            assert found.result == expectedResult
//            String foundTrace = trim(found.trace)
//            println "[[[${foundTrace}]]]"
//            println ">>>$expectedTrace<<<"
//            println "Found.  length: ${foundTrace.size()}, type:${foundTrace.class}"
//            println "expected.  length: ${expectedTrace.size()}, type:${expectedTrace.class}"
//            assert expectedTrace == foundTrace
        }
	
	def "should be able to process a file and a middle test of a middle set should be as expected"(){
		setup:
			String expectedName = 'WiFiDHCPTests.4:'
			String expectedDesc = 'Address Lease Valid.'
			String expectedResult = 'OK'
			String expectedTrace = 'Could not find the DHCP lease information'
			DeviceTestContainer container = service.processFile(dataFile)
			int testSetIndex = 2
			int testIndex = 3
		when:
			TestItem found = container.testData[testSetIndex].tests[testIndex]		
		then:	
			assert expectedName == found.name
			assert expectedResult == found.result
			assert expectedResult == found.result
			assert found.trace.contains(expectedTrace)
	}
}